public class Telephone {
    String manufacturer;
    String color;
    String produceDate;
    String type;
    public Telephone(String manufacturer,String color, String produceDate,String type){
        this.manufacturer = manufacturer;
        this.color = color;
        this.produceDate = produceDate;
        this.type = type;
    }

    public String getManufacturer(){
        return manufacturer;
    }
    public String getColor(){
        return color;
    }
    public String getProduceDate(){
        return produceDate;
    }
    public String getType(){
        return type;
    }
    @Override
    public String toString() {
        return "{" + "Manufacturer='" + manufacturer + '\'' +
                ", color='" + color + '\'' +
                ", Produce Date=" + produceDate +
                ", type=" + type +
                '}';
    }
}
