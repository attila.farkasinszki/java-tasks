import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class XMLDomProcessing extends DefaultHandler {
    private List<Telephone> myTelephones;
    private Document dom;

    public XMLDomProcessing(){
        myTelephones = new ArrayList<Telephone>();
    }

    public void runExample() {
        parseXmlFile();
        parseDocument();
        printData();
    }

    private void parseXmlFile(){
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder db = dbf.newDocumentBuilder();
            dom = db.parse("Telephone.xml");
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (SAXException se) {
            se.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    private void parseDocument(){
        Element docEle = dom.getDocumentElement();
        NodeList nl = docEle.getElementsByTagName("Telephone");
        if(nl != null && nl.getLength()> 0){
            for(int i = 0;i<nl.getLength();i++){
                Element el = (Element) nl.item(i);
                Telephone e = getTelephone(el);
                myTelephones.add(e);
            }
        }
    }

    private Telephone getTelephone(Element telEl){
        String manufacturer = getTextValue(telEl, "Manufacturer");
        String color = getTextValue(telEl, "Color");
        String produceDate = getTextValue(telEl, "ProduceDate");
        String type = telEl.getAttribute("type");
        Telephone e = new Telephone(manufacturer,color,produceDate,type);
        return e;
    }

    private String getTextValue(Element tele, String tagName){
        String textVal = null;
        NodeList nl = tele.getElementsByTagName(tagName);
        if(nl != null && nl.getLength()> 0){
            Element tel = (Element) nl.item(0);
            textVal = tel.getFirstChild().getNodeValue();
        }
        return textVal;
    }
    private int getIntValue(Element tele, String tagName){
        return Integer.parseInt(getTextValue(tele,tagName));
    }
    private void printData(){
        System.out.println("No of Telephone '" + myTelephones.size() + "'.");
        Iterator<Telephone> it = myTelephones.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
    }
}
