import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class XMLSaxProcessing extends DefaultHandler {
    private List<Telephone> myPhones;
    private String tempVal;
    private Telephone tempPhone;

    public XMLSaxProcessing(){
        myPhones = new ArrayList<Telephone>();
    }
    public void runExample(){
        parseDocument();
        printData();
    }
    public void parseDocument(){
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try{
            SAXParser sp = spf.newSAXParser();
            sp.parse("Telephone.xml",this);
        } catch (SAXException se) {
            se.printStackTrace();
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    private void printData(){
        System.out.println("No of Telephones'" + myPhones.size() + "'.");
        Iterator<Telephone> it = myPhones.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
    }

    public void startElement(String uri, String localName, String qname, Attributes attribute) throws SAXException{
        tempVal = "";
        if(qname.equalsIgnoreCase("Telephone")){
            tempPhone = new Telephone();
            tempPhone.setType(attribute.getValue("type"));
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        tempVal = new String(ch, start, length);
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("Telephone")) {
            myPhones.add(tempPhone);
        } else if (qName.equalsIgnoreCase("Manufacturer")) {
            tempPhone.setManufacturer(tempVal);
        } else if (qName.equalsIgnoreCase("Color")) {
            tempPhone.setColor(tempVal);
        } else if (qName.equalsIgnoreCase("ProduceDate")) {
            tempPhone.setProduceDate(tempVal);
        } else if(qName.equalsIgnoreCase("type")){
            tempPhone.setType(tempVal);
        }
    }
}
