public class Telephone {
    String manufacturer;
    String color;
    String produceDate;
    String type;

    public Telephone(){

    }
    public Telephone(String manufacturer,String color, String produceDate,String type){
        this.manufacturer = manufacturer;
        this.color = color;
        this.produceDate = produceDate;
        this.type = type;
    }

    public String getManufacturer(){
        return manufacturer;
    }
    public String getColor(){
        return color;
    }
    public String getProduceDate(){
        return produceDate;
    }
    public String getType(){
        return type;
    }

    public void setManufacturer(String manufacturer){
        this.manufacturer = manufacturer;
    }
    public void setColor(String color){
        this.color = color;
    }
    public void setProduceDate(String produceDate){
        this.produceDate = produceDate;
    }
    public void setType(String type){
        this.type = type;
    }
    @Override
    public String toString() {
        return "{" + "Manufacturer='" + manufacturer + '\'' +
                ", color='" + color + '\'' +
                ", Produce Date=" + produceDate +
                ", type=" + type +
                '}';
    }
}
