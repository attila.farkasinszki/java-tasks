class OutOfCoffeException extends RuntimeException{
    public OutOfCoffeException(String message){
        super(message);
    }
}
