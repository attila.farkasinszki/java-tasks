import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class OutOfCoffee {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, String> selection = new HashMap<>() {{
            put("Espresso", "100,0");
            put("Capuccino", "120,10");
            put("Latte", "150,10");
            put("Macchiato", "200,10");
            put("Latte macchiato", "200,10");
            put("Americano", "200,10");
            put("Melange", "220,10");
        }};
        System.out.println(selection.keySet());
        System.out.println("Which coffee do you want?");
        String input = scanner.nextLine();
        if (selection.containsKey(input)) {
            buyCoffee(selection, input);
        } else {
            throw new IllegalArgumentException();
        }
        System.out.println(selection);
    }

    public static void buyCoffee(Map<String,String> coffeeSelection,String user_input ) {
        String[] price_amount = coffeeSelection.get(user_input).split(",");
        System.out.println(price_amount[0] + " Huf");
        int amount = Integer.parseInt(price_amount[1]);
        try{
            if(amount == 0){
                throw new OutOfCoffeException("Out of this coffee");
            }
        } catch (OutOfCoffeException e){
            System.out.println(e.getMessage());
        }
        amount -= 1;
    }
}

