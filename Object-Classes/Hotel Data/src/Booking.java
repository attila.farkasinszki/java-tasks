public class Booking extends Object {
    private String name;
    private int phoneNumber;
    private String checkInDate;
    private String checkOutDate;
    private int numberOfGuests;

    public Booking(String name, int phoneNumberParam, String checkInDate, String checkOutDate, int numberOfGuests) {
        this.name = name;
        phoneNumber = phoneNumberParam;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        this.numberOfGuests = numberOfGuests;
        getData();
    }

    public String getData() {
        String result = "Name: " + name + "\nPhone: " + phoneNumber + " Check in date: " + checkInDate + " Check out date: " + checkOutDate + " Number of guests: " + numberOfGuests;
        return result;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "name='" + name + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", checkInDate='" + checkInDate + '\'' +
                ", checkOutDate='" + checkOutDate + '\'' +
                ", numberOfGuests=" + numberOfGuests +
                '}';
    }
}
