import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class HotelData {
    public static void main(String[] args) {
        booked_data();
    }

    public static void booked_data() {
        try {
            List<String> lines = Files.readAllLines(Paths.get("objects-classes_tasks_resources_hotel-data.csv"));
            for (String line : lines) {
                String[] entry = line.split(",");
                Booking guest = new Booking(entry[0], Integer.parseInt(entry[1]), entry[2], entry[3], Integer.parseInt(entry[4]));
                System.out.println(guest.getData());
                System.out.println(guest);
            }
        } catch (IOException e) {
            System.out.println("No such filename: " + e.getMessage());
        }
    }
}
