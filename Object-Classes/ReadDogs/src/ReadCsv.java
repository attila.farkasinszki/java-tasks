import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ReadCsv {
    public List<AbstractDog> readLines(String filename) {

        List<AbstractDog> dog = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get(filename));
            for (String line : lines) {
                String[] splitted = line.split(", ");
                String type = splitted[0].trim();
                String name = splitted[1].trim();
                String gender = splitted[2].trim();
                LocalDate birthday = LocalDate.parse(splitted[3]);
                Double weight = Double.parseDouble(splitted[4]);
                if (type.equals("Bulldog")) {
                    dog.add(new Bulldog(name, gender, birthday, weight));
                } else if (type.equals("YorkshireTerrier")) {
                    dog.add(new YorkshireTerrier(name, gender, birthday, weight));
                } else if (type.equals("CaucasianShepherd")) {
                    Set<String> protect = new HashSet();
                    if (!splitted[5].equals("-")) {
                        for (int i = 5; i < splitted.length; i++) {
                            protect.add(splitted[i]);
                        }
                    }
                    dog.add(new CaucasianShepherd(name, gender, birthday, weight, protect));
                } else {
                    System.out.println("This dog shouldn't be here");
                }
            }
            return dog;
        } catch (
                IOException e) {
            throw new RuntimeException(e);
        }
    }
}
