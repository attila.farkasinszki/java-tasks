import java.time.LocalDate;
import java.util.Random;

public class Bulldog extends AbstractDog {
    public Bulldog(String name, String gender, LocalDate birthDay, double weight) {
        super(name, gender, birthDay, weight);
    }

    @Override
    public String getType() {
        return "Bulldog";
    }

    @Override
    public String doSomethingBad() {
        Random random = new Random();
        if (random.nextDouble() < 0.3) {
            return killTheCat();
        } else {
            return destroyTheKitchen();
        }
    }

    public String killTheCat() {
        return "I killed the cat";
    }

    public String destroyTheKitchen() {
        return "Basically I broke everything and ate everything from the fridge";
    }

}
