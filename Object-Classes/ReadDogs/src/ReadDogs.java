import java.util.List;

public class ReadDogs {
    public static void main(String[] args){
        ReadCsv reader = new ReadCsv();
        List<AbstractDog> dogs = reader.readLines("dogs.csv");
        System.out.println(dogs);
    }
}
