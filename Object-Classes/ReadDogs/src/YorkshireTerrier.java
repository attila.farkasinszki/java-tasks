import java.time.LocalDate;
import java.util.Random;

public class YorkshireTerrier extends AbstractDog {
    public YorkshireTerrier(String name, String gender, LocalDate birthday, double weight){
        super(name,gender,birthday,weight);
    }
    public String chaseCat(){
        return "Chasing cat!";
    }
    public String ateChair(){
        return "I ate the chair";
    }
    public String shreadThePillow(){
        return "I was nervous because nobody played with me so I shredded the pillow!";
    }
    @Override
    public String getType(){
        return "Yorkshire Terrier";
    }
    public String doSomethingBad(){
        Random random = new Random();
        if (random.nextDouble() < 0.33){
            return chaseCat();
        } else if(random.nextDouble() < 0.66){
            return ateChair();
        } else {
            return shreadThePillow();
        }
    }
}
