public class Registration {
    private String e_mail;
    private String name;
    private String birth_date;
    private String workplace;
    private String phoneNumber;

    public Registration(String e_mail,String name, String birth_date,String workplace,String phoneNumber){
        this.e_mail = e_mail;
        this.name = name;
        this.birth_date = birth_date;
        this.workplace = workplace;
        this.phoneNumber = phoneNumber;
    }
    public void getData(){
        System.out.println("E-mail: " + e_mail);
        System.out.println("Name: " + name);
        System.out.println("Birth date: " + birth_date);
        System.out.println("Workplace: " + workplace);
        System.out.println("Phone number: " + phoneNumber);
    }
    public String getName(){
        return name;
    }
    public String getE_mail(){
        return e_mail;
    }
    public String getBirth_date(){
        return birth_date;
    }
    public String getWorkplace(){
        return workplace;
    }
    public String getPhoneNumber(){
        return phoneNumber;
    }
    public void setName(String newName){
        this.name = newName;
    }
    public void setE_mail(String newE_mail){
        this.e_mail = newE_mail;
    }
    public void setBirth_date(String newBirth_date){
        this.birth_date = newBirth_date;
    }
    public void setWorkplace(String newWorkplace){
        this.workplace = newWorkplace;
    }
    public void setPhoneNumber(String newPhoneNumber){
        this.phoneNumber = newPhoneNumber;
    }
}
