import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter your name: ");
        String name = scanner.nextLine();
        System.out.println("Please enter your e-mail: ");
        String e_mail = scanner.nextLine();
        System.out.println("Please enter your birth date: ");
        String birth_date = scanner.nextLine();
        System.out.println("Please enter your phone number: ");
        String phoneNumber = scanner.nextLine();
        System.out.println("Please enter your workplace: ");
        String workPlace = scanner.nextLine();
        Registration registration = new Registration(e_mail, name, birth_date, workPlace, phoneNumber);
        if (name.isEmpty() || e_mail.isEmpty() || birth_date.isEmpty()) {
            registration.getData();
            System.out.println("The email, name, and birth date must not be empty!");
        } else {
            registration.getData();
        }
    }
}
