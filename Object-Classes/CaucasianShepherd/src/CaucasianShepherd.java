import java.time.LocalDate;
import java.util.Set;

public class CaucasianShepherd extends AbstractDog {
    public Set<String> protect;

    public CaucasianShepherd(String name, String gender, LocalDate birthday,double weight,Set<String> protect){
        super(name,gender,birthday,weight);
        this.protect = protect;
    }
    public boolean protectFrom(String animal){
        if(protect.contains(animal)){
            return true;
        } else {
            return false;
        }
    }
    @Override
    public String doSomethingBad(){
        return "I killed a sheep";
    }
    public String getType(){
        return "Caucasian Shepherd";
    }
}
