import java.util.ArrayList;
import java.util.List;

public class Board {
    private List<Cell> filledCells;
    private int width;
    private int height;

    public Board(int width,int height){
        this.width = width;
        this.height = height;
        this.filledCells = new ArrayList<>();
    }
    public boolean fillCell(Cell cell){
        if((cell.getY() < 0 || cell.getY() > height) || (cell.getX() < 0 || cell.getX() > width )) {
            System.out.println("Your position outside of the board");
            return false;
        } else if (alreadyMarked(cell)){
            System.out.println("This cell is already filled!");
            return false;
        } else {
            filledCells.add(cell);
            System.out.println(cell + " successfully filled");
            return true;
        }
    }
    public List<Cell> getFilledCells() {
        return filledCells;
    }

    private boolean alreadyMarked(Cell cell){
        if(filledCells.contains(cell)){
            return true;
        } else {
            return false;
        }
    }
}
