import java.util.Scanner;

public class AmoebaTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Board width: ");
        int width = scanner.nextInt();
        System.out.println("Board height: ");
        int height = scanner.nextInt();
        Board board = new Board(width, height);
        while (board.getFilledCells().size() < width * height) {
            boolean player = false;
            while (player != true) {
                String marker = "X";
                System.out.println("Next player: " + marker);
                System.out.println("X position: ");
                int x = scanner.nextInt();
                System.out.println("Y position: ");
                int y = scanner.nextInt();
                Cell cell = new Cell(x, y, marker);
                if (board.fillCell(cell)) {
                    break;
                } else {
                    continue;
                }
            }
            while (player != true) {
                String marker = "Y";
                System.out.println("Next player: " + marker);
                System.out.println("X position: ");
                int x = scanner.nextInt();
                System.out.println("Y position: ");
                int y = scanner.nextInt();
                Cell cell = new Cell(x, y, marker);
                if (board.fillCell(cell)) {
                    break;
                } else {
                    continue;
                }
            }
        }
    }
}

