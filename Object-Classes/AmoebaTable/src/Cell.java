import java.util.Objects;

public class Cell {
    private int xPosition;
    private int yPosition;
    private String mark;

    public Cell(int xPosition,int yPosition,String mark){
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.mark = mark;
    }
    public int getX(){
        return xPosition;
    }
    public int getY(){
        return yPosition;
    }
    public String getMark(){
        return mark;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return Objects.equals(xPosition, cell.xPosition) &&
                Objects.equals(yPosition, cell.yPosition);
    }
    @Override
    public String toString() {
        return "Cell[x=" +
                xPosition + ", y=" + yPosition +
                ", mark=" + mark + "]";
    }
}
