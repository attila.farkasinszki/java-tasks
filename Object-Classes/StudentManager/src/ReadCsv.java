
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ReadCsv  {
    public void saveCsv(String folder,String filename,StringBuilder student){
        try{
            Files.write(Paths.get(folder,filename), student.toString().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            System.out.println("Can't write it to a file");
        }
    }
    public List<String> studentReader(String folder){
        List<String> student = new ArrayList<>();
        File dir = new File (folder);
        for(File file : dir.listFiles()) {
            String file_name = file.toString();
            try {
                List<String> lines = Files.readAllLines(Paths.get(file_name));
                for (String line:lines){
                    student.add(line);
                }
            } catch (IOException e){
                System.out.println("Can't reach the file!");
            }
        }
        return student;
    }
}
