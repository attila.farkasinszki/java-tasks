import java.time.LocalDate;

public class Student {
    private String name;
    private LocalDate birthday;
    private String classId;

    public Student(String name, LocalDate birthday, String classId){
        this.name = name;
        this.birthday = birthday;
        this.classId = classId;
    }
    public String getName(){
        return name;
    }
    public LocalDate getBirthday(){
        return birthday;
    }
    public String getClassId() {
        return classId;
    }
    public String toString() {
        return  "name='" + name +
                ", birthday=" + birthday +
                ", classId='" + classId;
    }
}
