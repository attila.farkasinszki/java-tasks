
import java.util.List;

public interface StudentRepository {
    void save(Student student);
    List<String> findAll();
}