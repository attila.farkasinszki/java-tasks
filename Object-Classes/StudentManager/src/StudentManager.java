
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

public class StudentManager {
    public static void main(String[] args){
        Scanner scanner = new Scanner((System.in));

        System.out.println("Folder name: ");
        String folder = scanner.nextLine();

        CsvStudentRepositry file = new CsvStudentRepositry(folder);
        System.out.println("Student name: ");
        String name = scanner.nextLine();

        System.out.println("Student birthday: ");
        String string_birthday = scanner.nextLine();
        LocalDate birthday = LocalDate.parse(string_birthday);

        System.out.println("Class ID: ");
        String classID = scanner.nextLine();

        Student student = new Student(name,birthday,classID);
        file.save(student);

        List<String> getStudent = file.findAll();
        System.out.println(getStudent);
    }
}
