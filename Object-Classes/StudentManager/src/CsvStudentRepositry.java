import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class CsvStudentRepositry implements StudentRepository {
    private String folder;

    public CsvStudentRepositry(String folder) {
        this.folder = folder;
        Path path = Paths.get(folder);
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                System.out.println("Can't reach the paths!");
            }
        }
    }

    public String getFolder(){
        return folder;
    }
    @Override

    public void save(Student student ){
        ReadCsv writer = new ReadCsv();
        StringBuilder studentData = new StringBuilder();
        studentData.append(student.getName() + ",  ");
        studentData.append(student.getBirthday() + ", ");
        studentData.append(student.getClassId());
        String filename = student.getName() + ".csv";
        writer.saveCsv(folder,filename,studentData);
    }
    public List<String> findAll() {
        ReadCsv readFolder = new ReadCsv();
        return readFolder.studentReader(folder);
    }
}

