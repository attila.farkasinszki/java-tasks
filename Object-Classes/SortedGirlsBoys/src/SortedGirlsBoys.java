import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortedGirlsBoys {
    public static void main(String[] args){
        List<People> sorted_list = new ArrayList<>();
        try{
            List<String> lines = Files.readAllLines(Paths.get("people.csv"));
            for(String line:lines){
                String[] splitted = line.trim().split(",");
                People people = new People(splitted[0],splitted[1],Integer.parseInt(splitted[2]));
                sorted_list.add(people);
            }
        } catch (IOException e) {
            System.out.println("Can't find the file!");
        }
        Collections.sort(sorted_list);
        Collections.reverse(sorted_list);
        System.out.println(sorted_list);
    }
}
