import java.util.ArrayList;
import java.util.List;

public class People implements Comparable<People> {
    public String name;
    public String gender;
    public int age;
    public List<People> peopleList;

    public People(String name, String gender, int age){
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.peopleList = new ArrayList<>();
    }
    public int getAge(){
        return age;
    }

    public String getGender() {
        return gender;
    }

    public int compareTo(People other){
        if(this.gender.equals("Female") && other.gender.equals("Male")){
            return 1;
        } else if(this.gender.equals(other.gender)){
            return this.age - other.age;
        } else {
            return -1;
        }
    }
    @Override
    public String toString() {
        return "Person{" +
                name + ", " + gender +
                ", " + age +
                '}';
    }
}
