package runner;

import Ordering.Order;
import Ordering.OrderManagementSystem;

import java.util.Scanner;

public class WebStore {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        String item = scanner.nextLine();
        Integer quantity = scanner.nextInt();
        Order order = new Order(item,quantity);
        OrderManagementSystem systemOrder = new OrderManagementSystem(order);
        System.out.println(systemOrder.getResult());
    }
}
