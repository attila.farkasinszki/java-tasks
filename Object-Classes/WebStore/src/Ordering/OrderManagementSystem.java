package Ordering;

public class OrderManagementSystem {

    private PaymentService paymentService;
    private Order order;

    public OrderManagementSystem(Order current_order) {
        paymentService = new PaymentService(current_order);
    }

    public String getResult() {
        String result = paymentService.getPrice();
        return result;
    }
}
