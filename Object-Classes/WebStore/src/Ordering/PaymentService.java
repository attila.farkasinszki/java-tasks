package Ordering;

public class PaymentService {

    private int quantity;
    private ProductAvailabilityService availabilityService;

    public PaymentService(Order current){
        this.quantity = current.getQuantity();
        availabilityService = new ProductAvailabilityService();
    }

    public String getPrice(){
        int price = (int) (Math.random()*100000+1);
        if(availabilityService.getAvailability()){
            return "Success! Thank you for your order. The price is " + price*quantity;
        } else {
            return "Something failed. Please contact us!";
        }
    }
}
