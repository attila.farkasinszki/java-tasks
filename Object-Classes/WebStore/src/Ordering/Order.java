package Ordering;

public class Order {
    String item;
    private Integer quantity;

    public Order(String item, Integer quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public String getItem() {
        return item;
    }

    public Integer getQuantity() {
        return quantity;
    }
}
