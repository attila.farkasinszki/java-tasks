Eg�sz�ts�k ki az el�z� programot �gy, hogy a titkos�tott f�jlokb�l a kulcs seg�ts�g�vel vissza tudja fejteni a program, hogy milyen titok van a f�jlokban. Amikor a decrypt parancsot �rjuk be, akkor a program egy f�jlt fog visszafejteni nek�nk.
A program l�p�sei:

megk�rdezi, hogy melyik f�jl tartalm�t szeretn�nk megtekinteni,
k�r egy kulcsot a f�jlhoz (ezt egy byte-k�nt fogjuk beolvasni ami egy sz�m -128 �s 127 k�z�tt),
a f�jlt tartalm�t beolvassuk egy byte[]-k�nt,
a kulcs seg�ts�g�vel visszafejtj�k, hogy mi is volt a titkos�tatlan tartalom (mivel a titkos�t�s sor�n hozz�adtuk a kulcs �rt�k�t a byte[] �rt�keihez, most csak ki kell azt vonnunk bel�l�k)
az eredm�nyt alak�tsuk egy UTF-8 k�dol�s� String objektumm�