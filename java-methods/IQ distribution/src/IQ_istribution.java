import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class IQ_istribution {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        System.out.println("File name: ");
        String file_name = scanner.nextLine();
        System.out.println("Upper bound: ");
        int upper_bound = scanner.nextInt();
        System.out.println(iqDistribution(file_name,upper_bound));
    }
    public static List<String> file_read(String file_name) {
        try{
            List<String> lines = Files.readAllLines(Paths.get(file_name));
            lines.remove(0);
            return lines;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static List<Integer> stringToInt(List<String> stringList){
        List<Integer> intData = new LinkedList<>();
        for(String nums:stringList) {
            for (String num : nums.split(",")) {
                if (!num.equals("")) {
                    Integer number = Integer.parseInt(num.trim());
                    intData.add(number);
                }
            }
        }
        return intData;
    }
    public static String iqDistribution(String file,int upper){
        Scanner scanner = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("#.####");

        List<Integer> numberList = stringToInt(file_read(file));
        float less =0;
        float sum = 0;
        for (int i = 0;i<numberList.size();i += 2){
            sum += numberList.get(i+1);
        }
        for(int i = 0;i<numberList.size();i += 2){
            if(numberList.get(i) <= upper){
                less += numberList.get(i+1);
            }
        }
        float result =  less /  sum *100;
        String output = df.format(result) + "% of the people have less than or equal IQ";
        return output;
    }
}
