public class ConsoleCircle {
    public static void main(String[] args){
        for(int i = 0;i< 20;i++){
            StringBuilder circle = new StringBuilder();
            for(int j = 0;j<20;j++){
                if(Math.sqrt((10 - i) * (10 - i) + (10 - j) * (10 - j)) < 6.5){
                    circle.append("oo");
                } else {
                    circle.append("--");
                }
            }
            System.out.println(circle + System.lineSeparator());
        }
    }
}