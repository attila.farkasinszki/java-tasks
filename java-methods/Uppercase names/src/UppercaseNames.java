import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UppercaseNames {
    public static void main(String[] args){
        List<String> names = Arrays.asList("Bob", "Adam", "Dave", "Jenny", "Scarlett", "Vanda", "Ann","Jenna", "Bill", "Steve", "Kelly", "Angelica", "Armstrong");
        ArrayList<String> upperNames = new ArrayList<>();
        for (String name:names ){
            upperNames.add(name.toUpperCase());
        }
        System.out.println(upperNames);
    }
}
