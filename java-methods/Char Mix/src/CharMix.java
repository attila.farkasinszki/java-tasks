import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class CharMix {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a text:");
        String text = scanner.nextLine();
        mix(text);
    }
    public static void mix(String word){
        List<Character> letter_list = new ArrayList<>();
        StringBuilder newText = new StringBuilder();
        for(int letter = 0;letter<word.length();letter++){
            letter_list.add(word.charAt(letter));
        }
        Collections.shuffle(letter_list);
        for(int letter = 0;letter<letter_list.size();letter++){
            newText.append(letter_list.get(letter));
        }
        System.out.println(newText);
    }
}
