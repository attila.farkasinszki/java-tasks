import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class LongestPeriod {
    public static void main(String[] args) {
        longest_period(stringToInt());
    }

    public static List<String> file_read() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("File name: ");
        String file_name = scanner.nextLine();
        try{
            List<String> lines = Files.readAllLines(Paths.get(file_name));
            return lines;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Integer> stringToInt() {
        List<String> employee_period = file_read();
        List<Integer> int_list = new LinkedList<>();
        for (String entry : employee_period) {
            for (String nums : entry.split(",")) {
                Integer num = Integer.parseInt(nums.trim());
                int_list.add(num);
            }
        }
        return int_list;
    }

    private static void longest_period(List<Integer> numbers){
        List<Integer> best_period = new LinkedList<>();
        int start = 0;
        int length = 1;
        int temp_length = 1;
        int temp_start = numbers.get(0);
        for (int i = 1; i < numbers.size(); i++) {
            if (numbers.get(i) > numbers.get(i - 1)) {
                temp_length += 1;
            } else if (numbers.get(i) <= numbers.get(i - 1)) {
                temp_start = numbers.get(i);
                temp_length = 1;
            }
            if (temp_length > length) {
                length = temp_length;
                start = temp_start;
            }
        }
        best_period.add(start);
        best_period.add(length);
        System.out.println("Best period start: " + best_period.get(0));
        System.out.println("Best period length: " + best_period.get(1));
    }
}
