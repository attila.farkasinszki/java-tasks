import java.util.Random;
import java.util.Scanner;

public class LMaze {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Rows: ");
        int rows = scanner.nextInt();
        System.out.println("Columns: ");
        int columns = scanner.nextInt();
        System.out.println("Probability: ");
        double probability = scanner.nextDouble();
        String space = " ";
        for (int row = 0; row < rows; row++) {
            System.out.println("");
            int count = row;
            System.out.print("|");
            if(count < rows / 2){
                for (int column = 0; column < columns / 2; column++) {
                    int rd = random.nextInt(100);
                    if (rd > 100 - probability * 100) {
                        space = "X";
                        System.out.print(space);
                    } else {
                        space = " ";
                        System.out.print(space);
                    }
                    System.out.print("|");
                }
            } else if (count>= rows/2) {
                for (int column = 0; column < columns; column++) {
                    int rd = random.nextInt(100);
                    if (rd > 100 - probability * 100) {
                        space = "X";
                        System.out.print(space);
                    } else {
                        space = " ";
                        System.out.print(space);
                    }
                    System.out.print("|");
                }
            }
        }
    }
}

