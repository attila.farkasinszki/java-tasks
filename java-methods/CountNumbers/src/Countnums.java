import java.util.Scanner;

public class Countnums {
    public static void main(String[] args){
        System.out.println(num_count(split_string()));
    }
    public static String[] split_string(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Numbers:");
        String numbers = scanner.nextLine();
        String[] splitted = numbers.split(",");
        return splitted;
    }
    public static String num_count(String[] list) {
        String maxnum = "0";
        int max = 0;
        for (String number : list) {
            int count = 0;
            for (String num : list) {
                if (Integer.parseInt(number) == Integer.parseInt(num)) {
                    count += 1;
                }
                if (count > max) {
                    max = count;
                    maxnum = num;
                }
            }
        }
        return maxnum;
    }
}
