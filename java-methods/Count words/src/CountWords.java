import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class CountWords {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        String file_name = scanner.nextLine();
        try {
            List<String> lines = Files.readAllLines(Paths.get(file_name));
            HashMap<String,Integer> count_words = new HashMap<>();
            for (String line : lines) {
                String noSpecialChar = line.replaceAll("[^\\p{L}\\s]", " ");
                String[] words = noSpecialChar.toLowerCase().split(" ");
                for(String word:words){
                    if(!count_words.containsKey(word) ){
                        count_words.put(word,1);
                    } else {
                        int old_value = count_words.get(word);
                        count_words.put(word,old_value + 1);
                    }
                }
            }
            count_words.remove("");
            System.out.println(count_words);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}