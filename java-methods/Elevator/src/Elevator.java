import java.util.Scanner;

public class Elevator {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        String floor = "  _ _ _ _ _";
        String level_face = "|     :)   |";
        String level = "|          |";
        for(int i = 6; i>0;i--){
            StringBuilder start = new StringBuilder();
            if(i > 1) {
                start.append(floor + System.lineSeparator() + level);
            } else {
                start.append(floor + System.lineSeparator() + level_face);
            }
            System.out.println(start);
        }
        System.out.println("Which level you want to go?");

        int user_input = scanner.nextInt()+1;
        for(int i = 6; i>0;i--){
            StringBuilder moving = new StringBuilder();
            if(i != user_input){
                moving.append(floor + System.lineSeparator() + level);
            } else {
                moving.append(floor + System.lineSeparator() + level_face);
            }
            System.out.println(moving);
        }
    }
}
