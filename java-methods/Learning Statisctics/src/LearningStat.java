import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class LearningStat {
    public static void main(String[] args) {
        HashMap<String, Integer> stats = new HashMap<>();
        Scanner scanner = new Scanner(System.in);
        List<String> learn_list = new ArrayList<>();

        String user_input;
        while( !"exit".equals(user_input = scanner.nextLine())){
            switch (user_input) {
                case "learn":
                    System.out.println("Learning type: ");
                    String learn_type = scanner.nextLine();
                    System.out.println("Duration in minutes:");
                    int duration = scanner.nextInt();
                    learn_list.add(learn_type + "," + duration);
                    break;
                case "learn-list":
                    for (String learnType:learn_list) {
                        System.out.println(learnType);
                    }
                    break;
                case "learn-stat":
                    for( String learning : learn_list) {
                        String[] entry = learning.split(",");
                        if (stats.containsKey(entry[0])) {
                            int old_value = stats.get(entry[0]);
                            stats.put(entry[0], old_value + Integer.parseInt(entry[1]));
                        } else {
                            stats.put(entry[0], Integer.parseInt(entry[1]));
                        }
                    }
                    System.out.println(stats.entrySet());
                    break;
            }
        }
    }
}
