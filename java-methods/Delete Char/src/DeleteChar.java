import java.util.Scanner;

public class DeleteChar {
    public static void main(String[] args) {
        System.out.println(removeChar(sentence()));
    }
    public static String sentence(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a text!");
        String sent = scanner.nextLine();
        return sent;
    }
    private static StringBuilder removeChar(String sentence){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which character you want to remove?");
        char character = scanner.next().charAt(0);
        StringBuilder newString = new StringBuilder(sentence);
        for(int i =0;i<newString.length();i++){
            if(character == newString.charAt(i)) {
                newString.deleteCharAt(i);
            }
        }
        return newString;
    }
}
