import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CollatzSequence {
    public static void main(String[] args) {
        List<Integer> collatzfunc = collatzList();
        System.out.println(collatzfunc);
        System.out.println(file(collatzfunc));
    }

    public static List<Integer> collatzList() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Give me a number");
        int input = scanner.nextInt();
        List<Integer> collatz = new ArrayList<>();
        collatz.add(input);
        for (int i = 0; collatz.get(i) != 1; i++) {
            if (collatz.get(i) % 2 == 0) {
                collatz.add(collatz.get(i) / 2);
            } else {
                collatz.add(3 * collatz.get(i) + 1);
            }
        }
        return collatz;
    }

    private static File file_reading(File file_name) {
        if (file_name.exists()) {
        }
        return file_name;
    }

    private static void file_writing(List<Integer> a, String file_name) {

        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(file_name))) {
            for (int i = 0; i < a.size(); i++) {
                String number = a.get(i).toString();
                writer.write(number + " ");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String file(List<Integer> a) {
        Scanner scanner = new Scanner(System.in);
        boolean loop = false;
        while(loop != true) {
            System.out.println("Would you like to save it to a file?");
            String answer = scanner.nextLine();
            if (answer.equals("yes")) {
                System.out.println("What file name you want?");
                String file_name = scanner.nextLine();
                File file = new File(file_name);
                if (file_reading(file).exists()) {
                    System.out.println("Would you like to overwrite your existing file?");
                    String answer1 = scanner.nextLine();
                    if (answer1.equals("yes")) {
                        file_writing(a, file_name);
                        break;
                    } else {
                        continue;
                    }
                } else {
                    file_writing(a, file_name);
                    loop = true;
                }
            }
        }
        return "File succesfully saved";
    }
}