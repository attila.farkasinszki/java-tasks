import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class DivideAll {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Numbers: ");
        String input = scanner.nextLine();
        System.out.println(stringtoint(input));
        divideAll(stringtoint(input));
    }
    public static List<Integer> stringtoint(String nums){
        ArrayList<Integer> ints = new ArrayList<>();
        for(String number: nums.split(",")){
            ints.add(Integer.parseInt(number));
        }
        return ints;
    }
    public static void divideAll(List<Integer> numbers){
        int divide = 0;
        for(int num:numbers){
            int count = 0;
            for(int ints: numbers){
                if(ints % num == 0){
                    count +=1;
                }
            }
            if(count == numbers.size()){
                divide = num;
                System.out.println("Divides All: " + divide);
            }
        }
    }
}
