import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class StartupShare {
    public static void main(String[] args){
        List<String> loggedHours = Arrays.asList(
                "Zuckerberg,4", "Steve,4", "Bill,1", "Bill,2", "Zuckerberg,4", "Bill,2",
                "Zuckerberg,1", "Zuckerberg,3", "Zuckerberg,1", "Zuckerberg,1", "Steve,4",
                "Bill,2", "Zuckerberg,3", "Bill,2", "Zuckerberg,4", "Steve,1",
                "Bill,3", "Steve,2", "Steve,2", "Bill,1", "Zuckerberg,4",
                "Bill,4", "Steve,4", "Bill,2", "Bill,3", "Zuckerberg,3",
                "Steve,4", "Zuckerberg,4", "Bill,2", "Zuckerberg,3"
        );
        HashMap<String,Integer> count_hour = new HashMap<>();
        for(String entry:loggedHours){
            String[] a = entry.split(",");
            if(count_hour.containsKey(a[0])){
                int old_value = count_hour.get(a[0]);
                count_hour.put(a[0],Integer.parseInt(a[1])+old_value);
            } else {
                count_hour.put(a[0], Integer.parseInt(a[1]));
            }
        }
        int sum = count_hour.get("Steve") + count_hour.get("Bill") + count_hour.get("Zuckerberg");
        System.out.println("Steve: " + ((float)count_hour.get("Steve")/sum*100));
        System.out.println("Bill: " + ((float)count_hour.get("Bill")/(float)sum)*100);
        System.out.println("Zuckerberg: " +((float)count_hour.get("Zuckerberg")/(float)sum)*100);
    }
}