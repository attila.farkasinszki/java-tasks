import java.util.Random;
import java.util.Scanner;

public class InjectChar {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a text:");
        String text = scanner.nextLine();
        System.out.println("Enter a letter:");
        String letter = scanner.nextLine();
        System.out.println(newString(text,letter));

    }
    public static StringBuilder newString(String sentence,String character){
        Random random = new Random();

        int position = random.nextInt(sentence.length());
        StringBuilder new_text = new StringBuilder(sentence).insert(position,character);
        return new_text;
    }

}
