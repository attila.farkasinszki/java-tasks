import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomID {
    public static void main(String[] args){
        List<String> alphabet = Arrays.asList("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z");
        List<String> numbers = Arrays.asList("0","1","2","3","4","5","6","7","8","9");
        System.out.println(nLong(alphabet));
        System.out.println(fiveString(alphabet));
        System.out.println(longID(alphabet,numbers));
    }
    public static String fiveString(List<String> characters){
        Collections.shuffle(characters);
        String fivechar = "";
        for (int i=0;i<5;i++){
            fivechar +=characters.get(i);
        }
        return fivechar;
    }

    public static String nLong(List<String> characters) {
        Collections.shuffle(characters);
        int number = (int) (Math.random() * 100);
        String longstring = "";
        if(number <= characters.size()) {
            for (int i = 0; i < number; i++) {
                longstring += characters.get(i);
            }
        } else {
            throw new IllegalArgumentException("Maximum size is 26");
        }
        return longstring;
    }
    public static StringBuilder longID(List<String> alphabet,List<String> zerotonine){
        StringBuilder id = new StringBuilder();
        int n = 0;
        for(int i = 0;i<5;i++) {
            if (i <= 2) {
                Collections.shuffle(alphabet);
                id.append(alphabet.get(n));
                id.append(alphabet.get(n+1));
                id.append(alphabet.get(n+2));
                id.append(alphabet.get(n+3));
                id.append("-");
            } else {
                Collections.shuffle(zerotonine);
                id.append(zerotonine.get(n));
                id.append(zerotonine.get(n+1));
                id.append(zerotonine.get(n+2));
                id.append(zerotonine.get(n+3));
                id.append("-");
            }
        }
        id.deleteCharAt( id.length()-1);
        return id;
    }
}
