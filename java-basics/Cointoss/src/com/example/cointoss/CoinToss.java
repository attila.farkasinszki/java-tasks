package com.example.cointoss;

import java.util.Scanner;
import java.util.Random;

public class CoinToss {
    public static void main(String[] args) {
        System.out.println("Guess one! Head or Tail?");
        Scanner scanner = new Scanner(System.in);
        String guess = scanner.nextLine();
        System.out.println(guess);
        Random random = new Random();
        int toss = random.nextInt(2);
        if ((toss == 1) && (guess.equals("head"))) {
            System.out.println("Congratulation! Yes it was head!");
        } else if ((toss == 0) && (guess.equals("head"))){
            System.out.println("Bad choice the correct answer is tail!");
        } else if ((toss == 0) && (guess.equals("tail"))){
            System.out.println("Congratulation! Yes it was tail!");
        } else if ((toss == 1) && (guess.equals("tail"))){
            System.out.println("Bad choice the correct answer is head!");
        }

    }
}
