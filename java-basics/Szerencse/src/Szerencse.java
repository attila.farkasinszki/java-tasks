import java.util.Random;

public class Szerencse {
    public static void main(String[] args) {
        Random random = new Random();
        int randomNumber = 1 + random.nextInt(6);
        switch (randomNumber) {
            case 1:
                System.out.println("Aki korán kel aranyat lel!");
                break;
            case 2:
                System.out.println("Ajándék csónak ne nézd a lapát!");
                break;
            case 3:
                System.out.println("Türelem tornaterem!");
                break;
            case 4:
                System.out.println("Addig jár a korsó a kútra amíg el nem törik!");
                break;
            case 5:
                System.out.println("Lassan járj tovább élsz!");
                break;
            case 6:
                System.out.println("A cél szentesíti az eszközt");
                break;
        }
    }
}
