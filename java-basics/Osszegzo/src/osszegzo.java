import java.util.Scanner;

public class osszegzo {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        boolean loop = true;
        int sum = 0;
        while(loop){
            System.out.println("Number: ");
            String number = input.nextLine();
            if(!number.equals("x")) {
                System.out.println(number);
                int yourNumber = Integer.parseInt(number);
                sum += yourNumber;
            } else {
                System.out.println(number);
                loop = false;
            }
        }
        System.out.println("Result: " + sum);
    }
}
