import java.util.Scanner;

public class Bottles {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        Boolean word = true;
        int green = 0;
        int black = 0;
        int reward = 10*green + 30*black;
        while (word){
        System.out.println("(Green / Black / X for finish):");
        String bootle = input.nextLine();
            if (bootle.equals("Green")) {
                green ++;
            } else if (bootle.equals("Black")){
                black ++;
            } else if (bootle.equals("X")){
                word = false;
            }
        }
        System.out.println("Overall reward: " + (black*30 + green*10));
    }
}
