import java.util.Scanner;

public class tokeletes {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int sum = 0;
        System.out.println("Please enter a number: ");
        int num = input.nextInt();
        for(int i=1; i<=num;i++) {
            if (num % i == 0) {
                sum += i;
            }
        }
        if(num*2 == sum){
            System.out.println("The number is perfect.");
        } else {
            System.out.println("The number is NOT perfect.");
        }
    }
}
