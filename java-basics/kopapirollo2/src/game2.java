import java.util.Random;
import java.util.Scanner;

public class game2 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int user_score = 0;
        int comp_score = 0;
        boolean game = false;
        while(game != true) {
            System.out.println("Guess one! (Rock / Paper / Scissors! Type 'X' to finish the game");
            String guess = scanner.nextLine();
            if (guess.equals("X")){
                game = true;
            } else if (guess.equals("Rock") || (guess.equals("Paper")) || (guess.equals("Scissors"))) {
                System.out.println(guess);
                int number = random.nextInt(3) + 1;
                String computer = "";
                if (number == 1) {
                    computer = "Rock";
                } else if (number == 2) {
                    computer = "Paper";
                } else {
                    computer = "Scissors";
                }
                if (((guess.equals("Rock")) && (computer.equals("Scissors")) || ((guess.equals("Paper")) && (computer.equals("Rock")) || ((guess.equals("Sciccors")) && (computer.equals("Paper")))))) {
                    System.out.println("Computer: " + computer);
                    System.out.println("You win!");
                    user_score += 1;
                } else if (((guess.equals("Rock")) && (computer.equals("Rock"))) || ((guess.equals("Paper")) && (computer.equals("Paper")) || ((guess.equals("Scissors")) && (computer.equals("Scissors"))))) {
                    System.out.println("Computer: " + computer);
                    System.out.println("It's a tie!");
                } else {
                    System.out.println("Computer: " + computer);
                    System.out.println("You lose!");
                    comp_score += 1;
                }
                System.out.println("User score: " + user_score);
                System.out.println("Computer score: " + comp_score);
            } else {
                System.out.println("You choose an invalid tool!!");
                user_score -= 1;
            }
        }
    }
}
